// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.teletype;

import static org.refcodes.cli.CliSugar.*;

import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;

import org.refcodes.archetype.C2Helper;
import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.ArgsFilter;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.BooleanLiterals;
import org.refcodes.data.Literal;
import org.refcodes.data.Scheme;
import org.refcodes.exception.BugException;
import org.refcodes.exception.Trap;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.properties.Properties;
import org.refcodes.properties.PropertiesBuilderImpl;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.HttpRestServer;
import org.refcodes.rest.RestRequestConsumer;
import org.refcodes.rest.RestRequestEvent;
import org.refcodes.rest.RestfulHttpClient;
import org.refcodes.rest.RestfulHttpServer;
import org.refcodes.serial.alt.tty.Parity;
import org.refcodes.serial.alt.tty.StopBits;
import org.refcodes.serial.alt.tty.TtyPort;
import org.refcodes.serial.alt.tty.TtyPortHub;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.HorizAlignTextBuilder;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.MediaType;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.UrlBuilder;

/**
 * Tool for communication, P2P and encryption as well as steganography.
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int DEFAULT_DATA_BITS = 8;
	private static final int DEFAULT_BAUD_RATE = 9600;

	private static final String TITLE = "<T.E.L.E.T.Y.P.E>";
	private static final String NAME = "teletype";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String DESCRIPTION = "Teleprinter command line tool for sending data to and receiving data from TTY (serial) ports simultaneously (see [https://www.metacodes.pro/manpages/teletype_manpage]).";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/teletype_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );

	private static final String MESSAGE_PROPERTY = "message";
	private static final String ASCIIZ_PROPERTY = "asciiz";
	private static final String LISTEN_PROPERTY = "listen";
	private static final String SEND_PROPERTY = "send";
	private static final String STOP_BITS_PROPERTY = "stopBits";
	private static final String PARITY_PROPERTY = "parity";
	private static final String DATA_BITS_PROPERTY = "dataBits";
	private static final String BAUD_PROPERTY = "baud";
	private static final String PORT_PROPERTY = "port";
	private static final String LIST_PROPERTY = "list";

	private static final String ASCIIZ_QUERY_FIELD = "asciiz";
	private static final int MAX_OPEN_SEND_DAEMON_RETRY_COUNT = 5;
	private static final String C2_SEND_ENDPOINT = "/send";
	private static final String C2_PORT_PROPERTY = "port";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static C2Helper _c2Helper = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {
		final Flag theListPortsFlag = flag( 'l', "list", LIST_PROPERTY, "List all detected TTY (COM) serial ports." );
		final StringOption theTtyPortArg = stringOption( 'p', "port", PORT_PROPERTY, "The COM (serial) port to operate on." );
		final IntOption theBaudArg = intOption( 'b', "baud", BAUD_PROPERTY, "The baud rate to use for the TTY (COM) serial port." );
		final IntOption theDataBitsArg = intOption( "data-bits", DATA_BITS_PROPERTY, "The data bits to use for the TTY (COM) serial port (usually a value of 7 or 8)." );
		final EnumOption<Parity> theParityArg = enumOption( "parity", Parity.class, PARITY_PROPERTY, "The parity to use for the TTY (COM) serial port: " + VerboseTextBuilder.asString( Parity.values() ) );
		final EnumOption<StopBits> theStopBitsArg = enumOption( "stop-bits", StopBits.class, STOP_BITS_PROPERTY, "The stop bits to use for the TTY (COM) serial port: " + VerboseTextBuilder.asString( StopBits.values() ) );
		final Flag theSendFlag = flag( 'S', "send", SEND_PROPERTY, "Send data to the COM (serial) port." );
		final Flag theListenFlag = flag( 'L', "listen", LISTEN_PROPERTY, "Listen for data from the COM (serial) port." );
		final Flag theAsciizFlag = flag( 'z', "asciiz", ASCIIZ_PROPERTY, "Terminate the message to be sent by the value 0 (\"zero\"), print line break upon an incoming 0 (\"zero\")." );
		final StringOption theMessageArg = stringOption( 'm', "message", MESSAGE_PROPERTY, "The message to be sent." );
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		final Flag theQuietFlag = quietFlag( true );
		final Flag theDebugFlag = debugFlag();
		final Flag theInitFlag = initFlag( false );
		final Flag theCleanFlag = cleanFlag( false );
		final ConfigOption theConfigArg = configOption();

		// @formatter:off
		final Term theArgsSyntax = cases(
			and( theListPortsFlag, any ( theQuietFlag, theDebugFlag ) ),
			and( theTtyPortArg, xor( theListenFlag, and( theSendFlag, theMessageArg ) ), any ( theBaudArg, theDataBitsArg, theStopBitsArg, theParityArg, theAsciizFlag, theConfigArg, theQuietFlag, theDebugFlag ) ),
			and( theConfigArg, xor( theListenFlag, and( theSendFlag, theMessageArg ) ), any ( theTtyPortArg, theBaudArg, theDataBitsArg, theStopBitsArg, theParityArg, theAsciizFlag, theQuietFlag, theDebugFlag ) ),
			and( theInitFlag, optional( theConfigArg, theQuietFlag ) ),
			and( theTtyPortArg, theCleanFlag, any( theQuietFlag, theDebugFlag ) ),
			xor( theHelpFlag, and( theSysInfoFlag, any( theQuietFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "List all available TTY (COM) ports", theListPortsFlag ),
			example( "Receive data from specific port", theTtyPortArg, theListenFlag),
			example( "Send data to specific port", theTtyPortArg, theSendFlag, theMessageArg),
			example( "Receive data from specific port using given config", theListenFlag, theConfigArg),
			example( "Send data to specific port using given config", theSendFlag, theMessageArg, theConfigArg),
			example( "Initialize default config file", theInitFlag ),
			example( "Initialize specific config file", theInitFlag, theConfigArg),
			example( "Clean the port's lock file", theCleanFlag, theTtyPortArg),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();
		final boolean isDebug = theArgsProperties.getBoolean( theDebugFlag );
		final boolean isAsciiz = theArgsProperties.getBoolean( theAsciizFlag );
		final boolean isVerbose = theCliHelper.isVerbose();

		try {
			if ( theListPortsFlag.isEnabled() ) {
				listSerialPorts( isVerbose );
			}
			else if ( theTtyPortArg.hasValue() ) {
				final int theBaudRate = theArgsProperties.getIntOr( theBaudArg, DEFAULT_BAUD_RATE );
				final int theDataBits = theArgsProperties.getIntOr( theDataBitsArg, DEFAULT_DATA_BITS );
				final String thePortName = theArgsProperties.get( theTtyPortArg );
				final Parity theParity = theArgsProperties.getEnumOr( theParityArg, Parity.NONE );
				final StopBits theStopBits = theArgsProperties.getEnumOr( theStopBitsArg, StopBits.AUTO );
				final TtyPort theTtyPort = new TtyPortHub().toPort( thePortName );
				if ( theTtyPort == null ) {
					throw new IllegalArgumentException( "No port <" + theTtyPortArg.getValue() + "> has been found." );
				}
				_c2Helper = C2Helper.builder().withInstanceAlias( theTtyPort.getAlias().toLowerCase() ).withResourceClass( Main.class ).withLogger( LOGGER ).withVerbose( isVerbose ).build();

				// CLEAN:
				if ( theCleanFlag.isEnabled() ) {
					_c2Helper.deleteLockFile( isVerbose );
				}
				// LISTEN:
				else if ( theListenFlag.isEnabled() ) {
					listen( theTtyPort, theBaudRate, theDataBits, theStopBits, theParity, isAsciiz, isVerbose, isDebug );
				}
				// SEND:
				else if ( theSendFlag.isEnabled() ) {
					sendDispatch( theTtyPort, theMessageArg.getValue(), theBaudRate, theDataBits, theStopBits, theParity, isAsciiz, isVerbose, isDebug );
				}
			}
			else {
				throw new BugException( "We should never end up here, please check your command line args!" );
			}
		}
		catch ( Exception e ) {
			theCliHelper.exitOnException( e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static void sendDispatch( TtyPort aTtyPort, String aMessage, int aBaud, int aDataBits, StopBits aStopBits, Parity aParity, boolean isAsciiz, boolean isVerbose, boolean isDebug ) throws IOException, HttpResponseException, ParseException {
		try ( TtyPort theTtyPort = aTtyPort.withOpen( aBaud, aDataBits, aStopBits, aParity ) ) {
			if ( isVerbose ) {
				LOGGER.printSeparator();
				LOGGER.info( "Sending \"" + aMessage + "\" to port <" + aTtyPort.getAlias().toUpperCase() + ">" + ( isAsciiz ? ", 0 (\"zero\") terminated " : " " ) + "..." );
			}
			sendMessage( aTtyPort, aMessage, isAsciiz );
		}
		catch ( IOException ttyExc ) {
			final Properties theLockProperties = _c2Helper.readLockFile( isVerbose );
			if ( theLockProperties != null ) {
				final int thePort = theLockProperties.getInt( C2_PORT_PROPERTY );
				if ( PortManagerSingleton.getInstance().isPortBound( thePort ) ) {
					if ( isVerbose ) {
						LOGGER.printSeparator();
						LOGGER.info( "Sending \"" + aMessage + "\" to port <" + aTtyPort.getAlias().toUpperCase() + ">" + ( isAsciiz ? ", 0 (\"zero\") terminated " : " " ) + "..." );
					}
					final RestfulHttpClient theClient = new HttpRestClient().withBaseUrl( Scheme.HTTP, Literal.LOCALHOST.getValue(), thePort );
					final UrlBuilder theUrl = new UrlBuilder( toSendEndpoint() );
					theUrl.getQueryFields().put( ASCIIZ_QUERY_FIELD, Boolean.toString( isAsciiz ) );
					final RequestHeaderFields theHeadeFields = new RequestHeaderFields();
					theHeadeFields.putContentType( MediaType.TEXT_PLAIN );
					theClient.doPost( theUrl, theHeadeFields, aMessage );
				}
			}
			else {
				throw new IllegalStateException( "The port <" + aTtyPort.getAlias() + "> seems to be in use by another (unknwon) application!" );
			}
		}
	}

	private static void sendDaemon( TtyPort aTtyPort, boolean isVerbose, boolean isDebug ) throws IOException, ParseException {
		final Properties theProperties = _c2Helper.readLockFile( isVerbose );
		if ( theProperties != null ) {
			final Integer thePort = theProperties.getInt( C2_PORT_PROPERTY );
			if ( thePort != null && !PortManagerSingleton.getInstance().isPortAvaialble( thePort ) ) {
				throw new IllegalStateException( "Cannot start daemon as the port <" + thePort + "> reserved by the lockfile is already in use!" );
			}
		}
		RestfulHttpServer theRestServer = null;
		int count = 1;
		int thePort;
		do {
			thePort = PortManagerSingleton.getInstance().bindAnyPort();
			try {
				theRestServer = new HttpRestServer().withOpen( thePort );
			}
			catch ( IOException e ) {
				if ( count > MAX_OPEN_SEND_DAEMON_RETRY_COUNT ) {
					throw e;
				}
			}
			count++;
		} while ( theRestServer == null );

		_c2Helper.writeLockFile( new PropertiesBuilderImpl().withPutInt( C2_PORT_PROPERTY, thePort ), isVerbose );
		theRestServer.onPost( toSendEndpoint(), new SendRequestConsumer( aTtyPort, isVerbose, isDebug ) ).open();
	}

	private static void sendMessage( TtyPort aTtyPort, String aMessage, boolean isAsciiz ) throws IOException {
		final OutputStream theOutputStream = aTtyPort.getOutputStream();
		theOutputStream.write( aMessage.getBytes() );
		if ( isAsciiz ) {
			theOutputStream.write( 0 );
		}
		theOutputStream.flush();
	}

	private static void listen( TtyPort aTtyPort, int aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, boolean isAsciiz, boolean isVerbose, boolean isDebug ) throws IOException, ParseException {
		try ( TtyPort theTtyPort = aTtyPort.withOpen( aBaudRate, aDataBits, aStopBits, aParity ) ) {
			sendDaemon( aTtyPort, isVerbose, isDebug );
			if ( isVerbose ) {
				LOGGER.printSeparator();
				LOGGER.info( "Listening on TTY port <" + aTtyPort.getAlias().toUpperCase() + "> with <" + aBaudRate + "> baud, <" + aDataBits + ">, data bits, <" + aStopBits + "> stop bits and a parity of <" + aParity + ">" + ( isAsciiz ? ", 0 (\"zero\") terminated " : " " ) + "..." );
				LOGGER.printTail();
			}
			byte eReceiveByte;
			while ( true ) {
				eReceiveByte = aTtyPort.receiveByte();
				if ( eReceiveByte == 0 && isAsciiz ) {
					System.out.println();
				}
				else {
					System.out.print( new String( new byte[] { eReceiveByte } ) );
				}
			}
		}
	}

	private static void listSerialPorts( boolean isVerbose ) throws IOException {
		final TtyPortHub theTtyPortHub = new TtyPortHub();
		final TtyPort[] thePorts = theTtyPortHub.ports();
		if ( isVerbose ) {
			if ( thePorts.length != 0 ) {
				int theMaxWidth = 0;
				int eWidth;
				for ( TtyPort ePort : thePorts ) {
					eWidth = ePort.getAlias().length();
					if ( theMaxWidth < eWidth ) {
						theMaxWidth = eWidth;
					}
				}
				final HorizAlignTextBuilder theBuilder = new HorizAlignTextBuilder().withColumnWidth( theMaxWidth );
				for ( TtyPort ePort : thePorts ) {
					LOGGER.info( "[" + theBuilder.toString( ePort.getAlias() ) + "] " + ePort.getName() + ": \"" + ePort.getDescription() + "\" (" + ePort.getPortMetrics().getBaudRate() + " baud)" );
				}
			}
			else {
				LOGGER.info( "No serial (COM) ports found." );
			}
		}
		else {
			for ( TtyPort ePort : thePorts ) {
				System.out.println( ePort.getAlias() + "\t" + ePort.getPortMetrics().getBaudRate() + " baud\t" + ePort.getDescription() + "\t" + ePort.getName() );
			}
		}
	}

	private static String toSendEndpoint() {
		return C2_SEND_ENDPOINT + _c2Helper.getInstanceAlias() != null ? ( "/" + _c2Helper.getInstanceAlias().toLowerCase() ) : "";
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private static class SendRequestConsumer implements RestRequestConsumer {

		private final TtyPort _ttyPort;
		boolean _isVerbose;
		boolean _isDebug;

		public SendRequestConsumer( TtyPort aTtyPort, boolean isVerbose, boolean isDebug ) {
			_ttyPort = aTtyPort;
			_isVerbose = isVerbose;
			_isDebug = isDebug;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onRequest( RestRequestEvent aRequest, HttpServerResponse aResponse ) throws HttpStatusException {
			final String aMessage = aRequest.getHttpBody();
			final String theAsciiz = aRequest.getUrl().getQueryFields().getFirst( ASCIIZ_QUERY_FIELD );
			final boolean isAsciiz = BooleanLiterals.isTrue( theAsciiz );
			try {
				sendMessage( _ttyPort, aMessage, isAsciiz );
			}
			catch ( IOException e ) {
				if ( _isVerbose ) {
					if ( _isDebug ) {
						System.err.println( "Cannot send the message \"" + aMessage + "\" to port <" + _ttyPort.getAlias().toUpperCase() + "> as of: " + Trap.asMessage( e ) );
						e.printStackTrace();
					}
					else {
						System.err.println( "Cannot send the message \"" + aMessage + "\" to port <" + _ttyPort.getAlias().toUpperCase() + "> as of: " + Trap.asMessage( e ) );
					}
				}
				else {
					if ( _isDebug ) {
						LOGGER.error( "Cannot send the message \"" + aMessage + "\" to port <" + _ttyPort.getAlias().toUpperCase() + "> as of: " + Trap.asMessage( e ), e );
					}
					else {
						LOGGER.error( "Cannot send the message \"" + aMessage + "\" to port <" + _ttyPort.getAlias().toUpperCase() + "> as of: " + Trap.asMessage( e ) );
					}
				}
			}
		}
	}
}